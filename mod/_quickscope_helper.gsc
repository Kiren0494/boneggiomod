#include maps\mp\_utility;
#include maps\mp\gametypes\_hud_util;
#include common_scripts\utility;

init()
{
	self endon("death");
	self endon("disconnect");
	
	for(;;)
	{
		wait 0.01;
		
		weapon = self getCurrentWeapon();
		if(IsSubStr( "l11", weapon ))
			continue;
		
		if ( self PlayerADS() < 0.60 )
			continue;
		
		targets = getTargetList();
		if ( targets.size == 0 )
			continue;
		
		targetsInReticle = [];
		foreach ( target in targets )
		{
			if ( !isDefined( target ) )
				continue;
			
			insideReticle = self WorldPointInReticle_Circle( target.origin, 65, 75 );
			
			if ( insideReticle )
				targetsInReticle[targetsInReticle.size] = target;
			
			
		}

		if ( targetsInReticle.size == 0 )
			continue;

		sortedTargets = SortByDistance( targetsInReticle, self.origin );
		self thread waitAndKillTarget( sortedTargets[0] );
		while(self WorldPointInReticle_Circle( sortedTargets[0].origin, 65, 75 ) && self PlayerADS() > 0.60 )
		{
			wait 0.01;
		}
		
		self notify("target_lost");
	}
}

waitAndKillTarget( target )
{
	self endon("death");
	self endon("disconnect");
	self endon("target_lost");
	
	self waittill("weapon_fired");
	
	origin = target getTagOrigin("j_spine4")+(0,0,-25);
	end = target getTagOrigin("j_spine4");
	
	MagicBullet( self getCurrentWeapon(), origin, end, self );	
}

getTargetList()
{
	targets = [];

	if ( level.teamBased )
	{		
		foreach( player in level.players )
		{
			if( player.team != self.team && player != self )
				targets[targets.size] = player;
		}

	}
	else
	{
		foreach( player in level.players )
		{
			if( player != self )
				targets[targets.size] = player;
		}
	}
	
	return targets;
}