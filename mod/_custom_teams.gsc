#include maps\mp\_utility;
#include maps\mp\gametypes\_hud_util;
#include common_scripts\utility;

precacheIWI() {
	precacheShader("repz_obama");
	precacheShader("repz_putin");
	precacheShader("repz_al_baghdadi");
}

init() {

	level.customTeam["axis"] = getTeam(0);
	level.customTeam["allies"] = getTeam(2);
}

getTeam(number) {

	team = [];

	switch(number) {

		case 0: team["name"] = "Islamic State";
				team["short_name"] = "ISIS";
				team["icon"] = "repz_al_baghdadi";
				team["eliminated"] = "Mujahideens have been eliminated";
				team["win_music"] = "mp_victory_opfor";
				break;
		case 1: team["name"] = "United States";
				team["short_name"] = "USA";
				team["icon"] = "repz_obama";
				team["eliminated"] = "USA soldiers have been eliminated";
				team["win_music"] = "mp_victory_sas";
				break;
		case 2: team["name"] = "Russia";
				team["short_name"] = "Russia";
				team["icon"] = "repz_putin";
				team["eliminated"] = "Russian soldiers have been eliminated";
				team["win_music"] = "mp_victory_sas";
				break;
	}

	return team;
}
