#include maps\mp\_utility;
#include maps\mp\gametypes\_hud_util;
#include common_scripts\utility;

init() {

	level thread mod\_settings::init();
	level thread mod\_custom_teams::init();
	level thread mod\_friend_hud::init();
	//level thread initTestClients(5);
}

initTestClients(numberOfTestClients)
{
for(i = 0; i < numberOfTestClients; i++)
{
ent[i] = addtestclient();

if (!isdefined(ent[i]))
{
wait 1;
continue;
}

ent[i].pers["isBot"] = true;
ent[i] thread initIndividualBot();
wait 0.1;
}
}

initIndividualBot()
{
self endon( "disconnect" );
while(!isdefined(self.pers["team"]))
wait .05;
self notify("menuresponse", game["menu_team"], "autoassign");
wait 0.5;
self notify("menuresponse", "changeclass", "class" + randomInt( 5 ));
self waittill( "spawned_player" );
self takeallweapons();
self giveweapon("usp_mp");
self switchtoweapon("usp_mp");
}
