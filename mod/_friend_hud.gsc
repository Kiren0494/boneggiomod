#include maps\mp\_utility;
#include maps\mp\gametypes\_hud_util;
#include common_scripts\utility;

init() {

	level precacheShaders();

	for(;;) {

		level waittill( "connected", player );
		player thread waitPlayerSpawn();
	}
}

waitPlayerSpawn() {

	self endon("disconnect");

	for(;;) {

		self waittill( "spawned_player" );
		self thread setupPictures();
		//self thread setupFriendWaypoints();
	}
}

precacheShaders() {

	precacheShader("repz_Kiren0494");
	precacheShader("repz_neneabc1");
	precacheShader("repz_Samjack");
	precacheShader("repz_toni1234");
	precacheShader("repz_Marchetto");
	precacheShader("repz_ghostxxxxx");

	//Party images
	precacheShader("repz_Samjack_party");
}

setupPictures() {

	allyY = 150;
	allyX = -65;
	enemyY = 150;
	enemyX = 65;

	foreach(player in level.players) {
		if(player.team == self.team) {
			self thread addPlayerPicture(player, allyX, allyY, "left");
			allyY = allyY + 45;
		} else {
			self thread addPlayerPicture(player, enemyX, enemyY, "right");
			enemyY = enemyY + 45;
		}
	}
	/*
	arrayAlly[0] = "cardicon_assad";
	arrayAlly[1] = "cardicon_burgertown";
	arrayAlly[2] = "cardicon_car";

	arrayEnemy[0] = "cardicon_claw";
	arrayEnemy[1] = "cardicon_weed";
	arrayEnemy[2] = "cardicon_tsunami";

	foreach(player in arrayAlly) {
		self thread addAllyPicture(player, allyX, allyY);
		allyY = allyY + 45;
	}

	foreach(player in arrayEnemy) {
		self thread addEnemyPicture(player, enemyX, enemyY);
		enemyY = enemyY + 45;
	}*/
}

addPlayerPicture(player, x, y, position) {

  portraitPicture = NewClientHudElem( self );
	portraitPicture.alignX = position;
	portraitPicture.alignY = position;
	portraitPicture.horzAlign = position;
	portraitPicture.vertAlign = position;
	portraitPicture.foreground = false;
	portraitPicture.hideWhenInMenu = true;
	portraitPicture.x = x;
	portraitPicture.y = y;
	portraitPicture.color = (255, 255, 255);
	portraitPicture setshader("repz_"+player.name, 35, 35);

	self thread watchPlayerHealth(player, portraitPicture);
	self thread watchPlayerDeath(player, portraitPicture);
	self thread destroyOnDeath(portraitPicture);
}

watchPlayerHealth(player, hudIcon) {

	self endon("death");
	self endon("disconnect");

	for(;;) {
		//player waittill("got_hitted");
		if(player.health == 0) {
			hudIcon.color = (0.75, 0.75, 0.75);
		} else {
			hudIcon.color = (1, player.health/100, player.health/100);
		}
		wait 0.2;
	}
}

watchPlayerDeath(player, hudIcon) {
	player waittill("death");
	//hudIcon setshader("black", 35, 35);
}

destroyOnDeath( hudIcon ) {
	self waittill("death");
	hudIcon destroy();
}

setupFriendWaypoints() {

	foreach(player in level.players) {
		if(player.team == self.team)
			self thread setHeadIcon( self, "repz_"+player.name, player, 50, 50, true );
	}
}

setHeadIcon( showTo, icon, player, width, height, keepPositioned ) {

	position = player getTagOrigin("j_head");

	headIcon = newClientHudElem( showTo );
	headIcon.archived = true;
	headIcon.x = position[0];
	headIcon.y = position[1];
	headIcon.z = position[2];
	headIcon.alpha = 0.85;
	headIcon setShader( icon, width, height );
	headIcon setWaypoint( true, true, true );

	for(;;) {
		position = player getTagOrigin("j_head")+(0,0,20);
		headIcon.x = position[0];
		headIcon.y = position[1];
		headIcon.z = position[2];
		wait 0.05;
	}
}
